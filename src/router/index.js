import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

import searchpage from '../components/searchpage.vue'
import HotelDetails from '../components/hotelDetails/HotelDetails.vue'
import Login from '../Authentication/Login.vue'
import Paymentpage from '../components/Reservation/Paymentpage.vue'
import Tripspage from '../components/Trip/Trpspage.vue'
import PaginationCom from '../components/Pagination/PaginationCom.vue'
import Dialog from '../components/Dialog/Dialog.vue'

import BookingSuccessful from '../components/Dialog/BookingSuccessful.vue';

import { isAuthenticate,isTokenExpired } from '@/Authentication/auth'

const routes = [
  {
    path: '/:name?/:isloggin?',
    name: 'home',
    component: HomeView
  },

  {
    path:'/searchpage/:id/:arrival/:dep/:adult?/:rooms?/:cityname',
   
    name:'searchpage',
    component:searchpage,
    // meta: { requiresAuth: true }
  },
  {
    // path:'/HotelDetails',
    path:'/HotelDetails/:id/:arrival/:dep/:rate',
    name:'HotelDetails',
    component:HotelDetails,
    meta: { requiresAuth: true }

  },
  {
    path:'/Login',
    name:'Login',
    component:Login
  },
  
  {
    path:'/trippage',
    name:'trippage',
    component:Tripspage,
  },
  {
    path:'/Paymentpage',
    name:'Paymentpage',
    component:Paymentpage,
  },




 
 
 
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !isAuthenticate()) {
   
    next('/login');
  } else if (to.meta.requiresAuth && isTokenExpired()) {
 
    logout();
    next('/login');
  } else {
   next()
  }
});

export default router
