let isAuthenticated = false;
let authToken = null;
let tokenExpiration=null

import { v4 as uuidv4 } from 'uuid';
export const login=()=> {

 
    isAuthenticated = true;
    authToken = generateToken();
    tokenExpiration = new Date();
    tokenExpiration.setHours(tokenExpiration.getHours() + 1);
    return true;

}

export const logout=()=> {
  isAuthenticated = false;
  authToken = null;
}

export const isAuthenticate=()=> {
    // isAuthenticated=false
    return isAuthenticated && !isTokenExpired();
}

export const  getToken=()=> {
  return authToken;
}

export function isTokenExpired() {
    return tokenExpiration && new Date() > tokenExpiration;
  }

const  generateToken=()=> {
  
//   return Math.random().toString(36).substr(2);
return  uuidv4();
}
