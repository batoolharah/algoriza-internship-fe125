
import {defineStore} from 'pinia'
import axios from "axios"

const key='9c4fdd55d7msh8d3ed034576f584p19db55jsn6c06d3ab594f'


export const useDataStore=defineStore('dataStore',{
    state:()=>({

        ListOfTrip:[],
        hotelData:[],
        // obj:[],
        Hotels:[], 
        srotbydata:[],
        hotel_rooms:[],
        sortbyData:[],
        bybudgetData:[],
        listofDes:[],
        tripList:[],
        totalPageNumber:'',
        seachData:{
          id:0,
          arr:'',
          dep:''
        },paymentData:{}
        
    }
    ),
    getters:{},
    actions: {
        async fetchData() {            
          const options = {
            method: 'GET',
            url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/searchDestination',
            params: {query: 'egypt'},
            headers: {
              'X-RapidAPI-Key': key,
              'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
            }
          };
          

            try {
              const response = await axios.request(options);
              console.log(response.data);
              const data=response.data.data
              this.hotelData=data
              // console.log("ID",data)
            } catch (error) {
              console.error(error);
            }
      },
      async getTotalPAgeNumber(ID,arrival,dep,page){
        const options = {
          method: 'GET',
          url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/searchHotels',
          params: {
            dest_id: ID,
            search_type: 'CITY',
            arrival_date: arrival,
            departure_date: dep,
            adults: '1',
            children_age: '0,17',
            room_qty: '1',
            page_number: page,
            languagecode: 'en-us',
            currency_code: 'AED'
          },
          headers: {
            'X-RapidAPI-Key': key,
            'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
          }
        };

    try {
      const response = await axios.request(options);

      console.log("response.data.data",response.data.data)
      
      this.totalPageNumber=response.data.data.meta[0].title
      console.log("response.data",response.data.data.meta[0])

    } catch (error) {
      console.error(error);
    }
      },
      async gethotelsData(ID,arrival,dep,page,adult){
        const options = {
          method: 'GET',
          url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/searchHotels',
          params: {
            dest_id: ID,
            search_type: 'CITY',
            arrival_date: arrival,
            departure_date: dep,
            adults: adult,
            children_age: '0,17',
            room_qty: '1',
            page_number: page,
            languagecode: 'en-us',
            currency_code: 'AED'
          },
          headers: {
            'X-RapidAPI-Key': key,
            'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
          }
        };

    try {
      const response = await axios.request(options);

      const HotelData=response.data.data
      console.log("response.data.data",response.data.data)
      console.log("this.totalPageNumber",this.totalPageNumber)
      this.Hotels=HotelData.hotels
      this.sortbyData=this.hotelData.hotels
      console.log("this ",HotelData)      

      
    } catch (error) {
      console.error(error);
    }
    },

   async getHotelDeatails(ID,arrivalDate,depDate){
    
  
    const options = {
      method: 'GET',
      url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/getHotelDetails',
      params: {
        hotel_id: ID,
        arrival_date: arrivalDate,
        departure_date: depDate,
        adults: '1',
        children_age: '1,17',
        room_qty: '1',
        languagecode: 'en-us',
        currency_code: 'EUR'
      },
      headers: {
        'X-RapidAPI-Key': key,
        'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
      }
    };
    try {
      const response = await axios.request(options);
      console.log("xxx",response.data);
      this.hotel_rooms=response.data.data
      console.log(this.hotel_rooms)

    } catch (error) {
      console.error(error);
    }
   },
    async getSortby(ID,s_type,arrival,dep){
     
     console.log("FORM STORE dep date",dep)
     console.log(this.seachData)
      const options = {
        method: 'GET',
        url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/getSortBy',
        params: {
          dest_id: ID,
          search_type: 'CITY',
          arrival_date: arrival,
          departure_date:dep,
          adults: '1',
          children_age: '1,17',
          room_qty: '1'
        },
        headers: {
          'X-RapidAPI-Key': key,
          'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
        }
      };
      try {
        const response = await axios.request(options);
        console.log(response.data);

        this.srotbydata=response.data.data
      } catch (error) {
        console.error(error);
      }
   },
   async getSortByFilter(ID,sortvalue,arr,dep){
    console.log("sortvalue",sortvalue)
   
    const options = {
      method: 'GET',
      url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/searchHotels',
      params: {
        dest_id: ID,
        search_type: 'CITY',
        arrival_date: arr,
        departure_date: dep,
        adults: '1',
        children_age: '0,17',
        room_qty: '1',
        page_number: '1',
        sort_by: sortvalue,
        languagecode: 'en-us',
        currency_code: 'AED'
      },
      headers: {
        'X-RapidAPI-Key': key,
        'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
      }
    };

      try {
        console.log("INDATA SOTR",sortvalue)
        const response = await axios.request(options);
        console.log("RESPONSE",response.data);
        // const HotelData=response.data.data
        // this.Hotels=response.data.data.hotels
        this.sortbyData=response.data.data.hotels
        console.log("response. of Srot",response.data.data)
    
        // this.sortbyData=HotelData.hotels
        // this.sortbyData=HotelData.hotels
        
        console.log("response. of Srot 555",this.sortbyData)
    

      } catch (error) {
        console.error(error);
      }
  },

    async getSortbybudget(ID,min,max,arr,dep) {
    

      const options = {
        method: 'GET',
        url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/searchHotels',
        params: {
          dest_id: ID,
          search_type: 'CITY',
          arrival_date: arr,
          departure_date: dep,
          adults: '1',
          children_age: '0,17',
          room_qty: '1',
          page_number: '1',
          price_min: min,
          price_max: max,
          languagecode: 'en-us',
          currency_code: 'AED'
        },
        headers: {
          'X-RapidAPI-Key':key,
          'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
        }
      };
      
      
      
      try {
        const response = await axios.request(options);
        console.log(response.data);
        // this.Hotels=response.data.data.hotels
        this.sortbyData=response.data.data.hotels
        console.log(this.Hotels)
        // const hoteldata=response.data.data
        // this.bybudgetData=hoteldata.hotels
        // this.Hotels=hoteldata.hotels
      } catch (error) {
        console.error(error);
      }
    },



    // setTripList(list){
    //   localStorage.setItem(list,JSON.stringify(this.tripList))
    // },
    // getTripList(list){
    //   this.tripList=JSON.parse(localStorage.getItem(list))
    // },
  
async getHotelDetailsDes(ID){
      
  const options = {
    method: 'GET',
    url: 'https://booking-com15.p.rapidapi.com/api/v1/hotels/getDescriptionAndInfo',
    params: {
      hotel_id: ID,
      languagecode: 'en-us'
    },
    headers: {
      'X-RapidAPI-Key': key,
      'X-RapidAPI-Host': 'booking-com15.p.rapidapi.com'
    }
  };;
  
try {
	const response = await axios.request(options);
	console.log(response.data);
  this.listofDes=response.data.data
  console.log("listofDes",this.listofDes);
} catch (error) {
	console.error(error);
}
  },

  addObject(obj) {
    this.ListOfTrip.push(obj);
    this.saveData();
  },
  // Mutation to save the data array to localStorage
  saveData() {
  //  localStorage.setItem('myData', JSON.stringify(this.ListOfTrip));
  localStorage.setItem('myData', JSON.stringify(this.ListOfTrip));
  },
  // Mutation to load the data array from localStorage
  loadData() {
    const savedData = localStorage.getItem('myData');
    this.ListOfTrip =  JSON.parse(savedData) ;
    console.log("SAVED",this.ListOfTrip)
  },
  setPaymentData(data){
    this.paymentData=data
  }
  
  },
    
});