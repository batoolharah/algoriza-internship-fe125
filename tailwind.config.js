/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors:{
        "bt-primary":"#2F80ED",
        "b-seondary":"#EBEBEB",
        "b-primary":"#4F4F4F",
        "secondary":"#F2F2F2",
        "border-bg":"#E0E0E0",
          "color-t":"#4F4F4F",
          "primary":"#BDBDBD"
      },
      backgroundImage: {
        'hero-pattern': "linear-gradient(180deg, #2969BF 0%, #144E9D 100%)",
        'bg-pattern': "linear-gradient(180deg, rgba(244, 244, 244, 0.00) 0%, #FFF 100%)",
        'gradient-pattern': 'linear-gradient(180deg, rgba(244, 244, 244, 0.00) 0%, #FFF 100%)',
        'semi':"rgba(0, 0, 0, 0.5)"
       
     },
     boxShadow:{
      "3xl": "0px 1px 0px 0px #E0E0E0" ,
     ' 2xl':" 0px 4px 4px 0px rgba(0, 0, 0, 0.25)"

     },
     dropShadow :{
       '3xl':" 0px 4px 14px rgba(0, 0, 0, 0.10)", 
     }
     
    },
  },
  plugins: [],
}

